
class Graph:
    def __init__(self, V, E):
        if self.checkCorrectness(V, E):
            self.V = V
            self.E = E

    @staticmethod
    def checkCorrectness(V, E):
        if type(V) not in (tuple, None) or type(E) not in (tuple, None):
            raise TypeError('V and E must be tuples')
        for edge in E:
            if type(edge) != tuple or len(edge) not in (2, 3):
                raise TypeError('Edges must be tuples - ("a", "b", 2)')
            if type(edge[0]) != str and type(edge[1]) != str:
                raise TypeError('Edges must consist of strings and integer - ("a", "b", 2)')
            if len(edge) == 3:
                if type(edge[2]) != int:
                    raise TypeError('Edges must consist of strings and integer - ("a", "b", 2)')
        return True

def main():
    pass


if __name__ == "__main__":
    main()