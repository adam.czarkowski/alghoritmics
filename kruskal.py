from graphs import Graph

class Kruskal(Graph):

    def __init__(self, V, E):
        super().__init__(V, E)
        self.forest = []

    def findMinimalSpanningTree(self):
        sortedEdges = self.boubleSortEdges(self.E)
        for edge in sortedEdges:
            self.forestExpanding(edge)
            for tree in self.forest:
                if len(tree["V"]) == len(self.V):
                    return tree

    def forestExpanding(self, e):
        for tree in self.forest:
            if e[0] in tree["V"] and e[1] in tree["V"]:
                return 0
            elif e[0] in tree["V"]:
                tree["V"].append(e[1])
                tree["E"].append(e)
                self.forestChecking(tree)
                return 0
            elif e[1] in tree["V"]:
                tree["V"].append(e[0])
                tree["E"].append(e)
                self.forestChecking(tree)
                return 0
        self.forest.append({ "V":[e[0], e[1]], "E":[e] })

    def forestChecking(self, t):
        for tree in self.forest:
            if tree != t:
                for vertex in tree["V"]:
                    if vertex in t["V"]:
                        for v in t["V"]:
                            if v not in tree["V"]:
                                tree["V"].append(v)
                        for e in t["E"]:
                            tree["E"].append(e)
                        self.forest.remove(t)
                        return 0

    def boubleSortEdges(self, E):
        sorted = [i for i in E]
        for i in range(len(sorted) - 1):
            for j in range(0, len(sorted) - i - 1):
                if sorted[j][2] > sorted[j + 1][2]:
                    sorted[j], sorted[j + 1] = sorted[j + 1], sorted[j]
        return sorted


kruskal1 = Kruskal(
    V=("a", "b", "c", "d", "e", "f"),
    E=(
            ("a", "b", 4),
            ("b", "c", 2),
            ("a", "e", 1),
            ("d", "e", 3),
            ("c", "d", 8),
            ("f", "d", 6),
            ("e", "f", 7),
            ("a", "f", 2),
            ("b", "e", 2)
        )
)
kruskal2 = Kruskal(
    V=("3", "0", "7", "4", "5", "2", "1", "6", "8"),
    E=(
            ("0", "1", 4),
            ("0", "7", 8),
            ("1", "2", 8),
            ("2", "3", 7),
            ("1", "7", 11),
            ("7", "8", 7),
            ("7", "6", 1),
            ("2", "8", 2),
            ("6", "5", 2),
            ("2", "5", 4),
            ("3", "5", 14),
            ("3", "4", 9),
            ("5", "4", 10),
            ("8", "6", 6)
        )
)

print(kruskal1.findMinimalSpanningTree())
print(kruskal2.findMinimalSpanningTree())

# Testing examples
# http://www.algorytm.org/klasyczne/minimalne-drzewo-rozpinajace.html
# https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/
