from graphs import Graph

class Prim(Graph):

    def __init__(self, V, E):
        super().__init__(V, E)
        self.selectedEdges = []
        self.tree = {"V": [], "E": []}

    def findMinimalSpanningTree(self):
        sortedEdges = self.boubleSortEdges(self.E)
        self.treeExpanding(sortedEdges[0])
        while len(self.tree["V"]) != len(self.V):
            self.selectedEdges = self.boubleSortEdges(self.selectedEdges)
            if not self.areVertexesInTree(self.selectedEdges[0]):
                self.treeExpanding(self.selectedEdges[0])
            del self.selectedEdges[0]
        return self.tree

    def treeExpanding(self, e):
        if e[0] in self.tree["V"]:
            self.tree["V"].append(e[1])
            self.updateEdgesList(e[1])
        elif e[1] in self.tree["V"]:
            self.tree["V"].append(e[0])
            self.updateEdgesList(e[0])
        else:
            self.tree["V"].append(e[0])
            self.tree["V"].append(e[1])
            self.updateEdgesList(e[0])
            self.updateEdgesList(e[1])
        self.tree["E"].append(e)

    def updateEdgesList(self, v):
        for e in self.E:
            if e not in self.tree["E"]:
                if e[0] in self.tree["V"] or e[1] in self.tree["V"]:
                    if not self.areVertexesInTree(e):
                        self.selectedEdges.append(e)

    def areVertexesInTree(self, e):
        if e[0] in self.tree["V"] and e[1] in self.tree["V"]:
            return True
        return False

    def boubleSortEdges(self, E):
        sorted = [i for i in E]
        for i in range(len(sorted) - 1):
            for j in range(0, len(sorted) - i - 1):
                if sorted[j][2] > sorted[j + 1][2]:
                    sorted[j], sorted[j + 1] = sorted[j + 1], sorted[j]
        return sorted


prim1 = Prim(
    V=("a", "b", "c", "d", "e", "f"),
    E=(
        ("a", "b", 4),
        ("b", "c", 2),
        ("a", "e", 1),
        ("d", "e", 3),
        ("c", "d", 8),
        ("f", "d", 6),
        ("e", "f", 7),
        ("a", "f", 2),
        ("b", "e", 2)
    )
)
prim2 = Prim(
    V=("3", "0", "7", "4", "5", "2", "1", "6", "8"),
    E=(
        ("0", "1", 4),
        ("0", "7", 8),
        ("1", "2", 8),
        ("2", "3", 7),
        ("1", "7", 11),
        ("7", "8", 7),
        ("7", "6", 1),
        ("2", "8", 2),
        ("6", "5", 2),
        ("2", "5", 4),
        ("3", "5", 14),
        ("3", "4", 9),
        ("5", "4", 10),
        ("8", "6", 6)
    )
)

print(prim1.findMinimalSpanningTree())
print(prim2.findMinimalSpanningTree())

# Testing examples
# http://www.algorytm.org/klasyczne/minimalne-drzewo-rozpinajace.html
# https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/
