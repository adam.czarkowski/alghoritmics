from graphs import Graph

class Euler(Graph):

    def __init__(self, V, E):
        super().__init__(V, E)

    def isGraphEulerian(self):
        for v in self.V:
            numberOfEdges = 0
            for e in self.E:
                if v in e:
                    numberOfEdges += 1
            if numberOfEdges % 2 != 0:
                return False
        return True

    def findEulerianCycle(self):
        if not self.isGraphEulerian():
            return None
        edgesList = list(self.E)
        stack = [self.V[0]]
        cycle = []
        while len(edgesList) != 0:
            for edge in edgesList:
                if stack[-1] in edge:
                    if edge[0] == stack[-1]:
                        stack.append(edge[1])
                    else:
                        stack.append(edge[0])
                    edgesList.remove(edge)
                    break
                elif edge == edgesList[-1]:
                    cycle.append(stack.pop())
        while len(stack) != 0:
            cycle.append(stack.pop())
        return cycle


euler1 = Euler(
    V=("a", "b", "c", "d", "e"),
    E=(
        ("a", "b"),
        ("a", "c"),
        ("b", "e"),
        ("b", "c"),
        ("b", "d"),
        ("e", "d")
    )
)

euler2 = Euler(
    V=("a", "b", "c", "d", "e"),
    E=(
        ("a", "b"),
        ("b", "c"),
        ("c", "d"),
        ("c", "e"),
        ("c", "d"),
        ("d", "e")
    )
)

euler3 = Euler(
    V=("a", "b", "c", "d", "e"),
    E=(
        ("a", "b"),
        ("b", "d"),
        ("a", "d"),
        ("d", "c"),
        ("d", "e"),
        ("e", "c")
    )
)

euler4 = Euler(
    V=("a", "b", "c", "d", "e", "f",
       "g", "h", "i", "j", "k"),
    E=(
        ("a", "b"),
        ("a", "c"),
        ("c", "b"),
        ("a", "d"),
        ("c", "d"),
        ("c", "e"),
        ("b", "e"),
        ("e", "f"),
        ("b", "f"),
        ("a", "g"),
        ("d", "h"),
        ("e", "h"),
        ("e", "i"),
        ("g", "j"),
        ("d", "j"),
        ("h", "j"),
        ("j", "k"),
        ("h", "k"),
        ("i", "k"),
        ("e", "k"),
    )
)

print(euler1.findEulerianCycle())
print(euler2.findEulerianCycle())
print(euler3.findEulerianCycle())
print(euler4.findEulerianCycle())

# Testing examples
# euler1 - http://www.algorytm.org/algorytmy-grafowe/cykl-eulera.html
# euler2, euler3 - https://eduinf.waw.pl/inf/alg/001_search/0134.php
# euler4 - http://www.student.krk.pl/026-Ciosek-Grybow/eulerowskie.html
